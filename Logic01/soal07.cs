﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base;

namespace Logic01
{
    class soal07 : LogicProps
    {
        public soal07()
        {
            Console.Write("Masukkan nilai : ");
            int n = int.Parse(Console.ReadLine());

            Array1D = new string[n];
            FillArray();

            LogicFunction.cetakarray1D(Array1D); //Cetak Array di reference dari LogicFunction

            Console.WriteLine("Press any key to continue!");
            Console.ReadKey();
        }
        private void FillArray()
        {
            int val = 2;
            int i = 2;
            for (int col = 0; col < Array1D.GetLength(0); col++)
            {   
                Array1D[col] = val.ToString();
                val = val * 2;
            }
        }
    }
}
