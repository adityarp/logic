﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Base;

namespace Logic01
{
    class soal04:LogicProps
    {
        public soal04()
        {
            Console.Write("Masukkan nilai n : ");
            int n = int.Parse(Console.ReadLine());

            Array2D = new string[1,n];
            FillArray();

            //printArray di reference
            LogicFunction.cetakarray2D(Array2D);

            Console.WriteLine("Press any key to continue!");
            Console.ReadKey();
        }
        private void FillArray()
        {
            int val = 1;
            for (int col = 0; col < Array2D.GetLength(1); col++)
            {
                Array2D[0, col] = val.ToString();
                val = val + 3;
            }
        }
    }
}
