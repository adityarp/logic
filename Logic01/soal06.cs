﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base;
namespace Logic01
{
    class soal06 : LogicProps
    {
        public soal06()
        {
            Console.Write("Masukkan nilai  : ");
            int n = int.Parse(Console.ReadLine());

            Array1D = new string[n];
            FillArray();

            //printArray di reference
            LogicFunction.cetakarray1D(Array1D);

            Console.WriteLine("Press any key to continue!");
            Console.ReadKey();
        }
        private void FillArray()
        {
            int val = 1;
            int i = 2;
            for (int col = 0; col < Array1D.GetLength(0); col++)
            {
                
                if (col == i)
                {
                    Array1D[col] = "*";
                    i = i + 3;
                }
                else
                {
                    val = (col * 4) + 1;
                    Array1D[col] = val.ToString();
                }
                
            }
        }
    }
}
