﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic03
{
    class soal03
    {
        public soal03()
        {
            Console.Write("Input Nilai Alice: ");
            string nilA = Console.ReadLine();
            Console.Write("Input Nilai Bob  : ");
            string nilB = Console.ReadLine();
            string[] arrNilA = nilA.Split(',');
            string[] arrNilB = nilB.Split(',');
            int Al = 0;
            int Bo = 0;
            for (int i = 0; i < arrNilA.Length; i++)
            {
                int A = Convert.ToInt32(arrNilA[i]);
                int B = Convert.ToInt32(arrNilB[i]);
                if (A > B)
                {
                    Al++;
                }
                else if (A < B)
                {
                    Bo++;
                }
            }
            Console.Write("Hasil: [" + Al + " , " + Bo + "]");
            Console.ReadKey();
        }
    }
}
