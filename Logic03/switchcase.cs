﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic03
{
    class switchcase
    {
        public switchcase()
        {
            Console.Write("Masukkan pilihan anda (1-3): ");
            string pilihan = Console.ReadLine();
            int i = Convert.ToInt16(pilihan);
            switch (i)
            {
                case 1: // jika nilai i = 1
                    Console.WriteLine("Anda memilih kucing"); //output jika i bernilai 1
                    break; // batas case 1, pemisah dengan case berikutnya
                case 2:
                    Console.WriteLine("Anda Memilih kelinci");
                    break;
                case 3:
                    Console.WriteLine("Anda memilih ikan");
                    break;

            }
            Console.ReadLine();
        }
    }
}
