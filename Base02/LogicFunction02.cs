﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base02
{
    public class LogicFunction02
    {
        public static void CetakArray1D (string[] arr)
        {
            for (int row = 0; row < arr.GetLength(0); row++)
            {
                Console.Write(arr[row] + "\t");
            }
            
        }
        public static void CetakArray2D(string[,] arr)
        {
            for (int row = 0; row < arr.GetLength(0); row++)
            {
                for (int col = 0; col < arr.GetLength(1); col++)
                {
                    Console.Write(arr[row, col] + "\t");
                }
                Console.Write("");
            }
          
        }
    }
}
