﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base02;

namespace Exam01
{
    class exam01 : LogicProps02
    {
        public exam01()
        {
            Console.WriteLine("masukan nilai : ");
            int n = int.Parse(Console.ReadLine());
            Array1D = new string[n];
            FillArray();
            LogicFunction02.CetakArray1D(Array1D);
            Console.ReadKey();
        }

        private void FillArray()
        {
            for (int col=0; col<Array1D.GetLength(0); col++)
            {
                Console.Write((col * 2) + 1 + "\t");
            }
        }
    }
}
