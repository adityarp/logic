﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Base
{
    public class LogicFunction
    {
        public static void cetakarray1D(string[] arr) //DIMENSI 1
        {
            for (int row = 0; row < arr.GetLength(0); row++)
            {
                Console.Write(arr[row] + "\t"); //perpindahan kolom
            }
            Console.WriteLine("\n"); //perpindahan baris 
        }

        public static void cetakarray2D(string[,] arr) //DIMENSI 2
        {
            for (int row = 0; row < arr.GetLength(0); row++)
            {
                for (int col = 0; col < arr.GetLength(1); col++)
                {
                    Console.Write(arr[row, col] + "\t"); //perpindahan kolom
                }
                Console.WriteLine("\n"); //perpindahan baris
            }
        }
        
        
    }
}
