﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base;

namespace Logic02
{
    class soal05 : LogicProps
    {
        public soal05(String inputKata)
        {
            Array1D = new String[inputKata.Length];//[i,j]
            FillArray(inputKata);
            LogicFunction.cetakarray1D(Array1D);
            KataArray();
            Console.WriteLine("\t");
            countupper();
            Console.ReadKey();
        }

        public void KataArray()
        {

            for (int i = 0; i < Array1D.Length; i++)
            {
                if (i == 0 ||
                    i == Array1D.Length - 1 ||
                    Array1D [i - 1] == " " ||
                    Array1D [i + 1] == " " || 
                    Array1D [i] == " ")
                    Console.Write(Array1D[i]);
                else
                    Console.Write("*");
            }
        }
        private void countupper()
        {
            int jmlUpper = 0;
            for (int i = 0; i < Array1D.Length; i++)
            {
                int temp = char.ConvertToUtf32(Array1D[i], 0);
                if (temp >= 65 && temp <= 90)
                    jmlUpper++;

            }
            Console.WriteLine("Jumlah Huruf Besar : " + jmlUpper.ToString());
            Console.ReadKey();
        }
        private void FillArray(string inputKata)
        {
            char[] cArr = inputKata.ToCharArray();
            int idx = 0;
            foreach(var ch in cArr)
            {
                Array1D[idx++] = ch.ToString();
            }
        }
    }
}