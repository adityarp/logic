﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base;

namespace Logic02
{
    class soalkal: LogicProps
    {
        public soalkal(string kalimat)
        {
            Array1D = new string[kalimat.Length];
            FillArray(kalimat);
            //Console.WriteLine(kalimat);
            Balik();
            Console.ReadKey();
        }

        private void Balik()
        {
            int b = (Array1D.Length - 1);
            for(int i=0; i< Array1D.Length; i++)
            {
                Console.Write(Array1D[b]);
                b--;
            }
        }

        private void FillArray(string kal)
        {
            char[] cArr = kal.ToCharArray();
            int idx = 0;
            foreach(var ch in cArr)
            {
                Array1D[idx++] = ch.ToString();
            }
        }
    }
}
