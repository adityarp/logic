﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base;

namespace Logic02
{
    class soal06: LogicProps
    {
        public soal06 (string kalimat)
        {
            Array1D = new string[kalimat.Length];
            FillArray(kalimat);
            LogicFunction.cetakarray1D(Array1D);
            countupper();
        }

        private void countupper()
        {
            int jmlUpper = 0;
            for(int i=0; i < Array1D.Length; i++)
            {
                int temp = char.ConvertToUtf32(Array1D[i], 0);
                if (temp >= 65 && temp <= 90)
                    jmlUpper++;
                
            }
            Console.WriteLine("Hasilnya : " + jmlUpper.ToString());
            Console.ReadKey();
        }

        private void FillArray(string kal)
        {
            //konversi string to array
            //char = kal.tochararray()
            char[] cArr = kal.ToCharArray();
            int idx = 0;
            foreach (var ch in cArr)
            {
                Array1D[idx++] = ch.ToString();
            }
        }
    }
}
